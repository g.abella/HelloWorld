﻿# Install chocolatey
if (Get-Command choco -ErrorAction SilentlyContinue)
{
    choco upgrade chocolatey
}
else
{
    iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex
}

# Install nuget
if (Get-Command NuGet.exe -ErrorAction SilentlyContinue)
{
    choco upgrade NuGet.CommandLine
}
else
{
    choco install NuGet.CommandLine
}
